json+hal for Play 2.1
================================

json+hal is a small module for the Play 2.1 framework that supports the [json+hal](http://stateless.co/hal_specification.html) specification.

It provides a series of case classes for deserializing link components including [URI templates](http://tools.ietf.org/html/rfc6570).

Example
-------


```
import com.atlassian.jsonhal._
import play.api.libs.json._


case class Foo(x: Int, _links: Links)
implicit val fooReads = Json.reads[Foo]


val json = """
{
 "x"      : 42,
 "_links" : { "self" :  {"href": "http://localhost:8080"},
              "blah" :  {"href": "http://localhost:8080/{blah}",
                         "templated" : true} 
            }
}
"""


val foo: JsResult[Foo] = Json.fromJson[Foo](Json.parse(json))
val jsonOut = Json.toJson(foo.get)
```

To Do
-----

* Add a body parser for the `application/json+patch` mime type
* Use `Validation` instead of `\/` for `Links` deserialization