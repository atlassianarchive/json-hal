package com.atlassian

import scalaz._, Scalaz._
import play.api.libs.json._
import play.api.libs.json.Json
import play.api.mvc._
import play.api.data.validation.ValidationError
import java.net.URI

package object jsonhal {

  private def err(s: String) = JsError(Seq(JsPath() -> Seq(ValidationError(s))))

  type JsErrors = Seq[(JsPath, Seq[ValidationError])] 
  type JsResultZ[+T] = JsErrors \/ T
  private val jsResultToEither = new (JsResult ~> JsResultZ) {
    def apply[A](a: JsResult[A]): JsResultZ[A] = \/.fromEither(a.asEither)
  }

  private val jsResultZToJsResult = new (JsResultZ ~> JsResult) {
    def apply[A](a: JsResultZ[A]): JsResult[A] = a.fold(
       l = JsError(_)
     , r = JsSuccess(_))
  }

  private def readFromString[T](f: String => T, errorMsg: String) = new Reads[T] {
    import scala.util.control.Exception._
    def reads(json: JsValue): JsResult[T] = {
      json match {
        case JsString(s) => (catching(classOf[Throwable]) opt (f(s))).cata(
          some = JsSuccess(_),
          none = err(errorMsg)
        )
        case _ => err(errorMsg)
      }
    }

  }

  implicit val uriReads: Reads[URI] = readFromString[URI](URI.create _, "validate.error.expected.uri")
  implicit val uriWrites: Writes[URI] = new Writes[URI] {
     def writes(u: URI): JsValue = JsString(u.toASCIIString)
  }  
  implicit val uriTemplateReads: Reads[URITemplate] = readFromString[URITemplate](URITemplate.apply _, "validate.error.expected.uri.template")
  implicit val uriTemplateWrites: Writes[URITemplate] = new Writes[URITemplate] {
    def writes(u: URITemplate): JsValue = JsString(u.input)
  }

  implicit val templatedLinkReads: Reads[TemplatedLink] = Json.reads[TemplatedLink]
 

  implicit val templatedLinkWrites: Writes[TemplatedLink] = new Writes[TemplatedLink] {
    val base: Writes[TemplatedLink] = Json.writes[TemplatedLink]
    def writes(l: TemplatedLink): JsValue = base.writes(l).asInstanceOf[JsObject] + ("templated", JsBoolean(true))
  }

  implicit val linkReads: Reads[Link] = Json.reads[Link]
  implicit val linkWrites: Writes[Link] = Json.writes[Link]

  implicit val linksWrites: Writes[Links] = new Writes[Links] {
    def writes(l: Links): JsValue = JsObject((l.std.mapValues(Json.toJson(_)) ++ l.templated.mapValues(Json.toJson(_))).toSeq)
  }

  implicit val linksReads: Reads[Links] = new Reads[Links] {
    def reads(json: JsValue): JsResult[Links] = {
      lazy val badLinksError = err("validate.error.expected.json+hal.links")
      def isTemplated(kv: (String, JsValue)) = (kv._2 \ "templated") == JsBoolean(true)
      def sequence[T](m: scala.collection.Map[String, JsResult[T]]): JsResult[Map[String, T]] = 
        jsResultZToJsResult(m.toList.map(kv => kv._2.map(v => (kv._1, v))).map(jsResultToEither).sequenceU).map(_.toMap)
      json match {
        case (obj: JsObject) => {
          for {
            std <- sequence[Link](obj.value.filterNot(isTemplated).mapValues(linkReads.reads(_)))
            templated <- sequence[TemplatedLink](obj.value.filter(isTemplated).mapValues(templatedLinkReads.reads(_)))
          } yield Links(std, templated)
        }
        case _ => badLinksError
      }
    }
  }

}