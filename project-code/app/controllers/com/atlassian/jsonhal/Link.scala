package com.atlassian.jsonhal

import scalaz._, Scalaz._
import java.net.URI
import play.api.libs.json._
import play.api.libs.json.Json, Reads.JsObjectReducer
import play.api.libs.functional.syntax._

case class Links(std: Map[String, Link] = Map(), templated: Map[String, TemplatedLink] = Map())

case class TemplatedLink(href: URITemplate
                       , name: Option[String] = None
                       , hreflang: Option[String] = None
                       , title: Option[String] = None)

case class Link(href: URI
              , name: Option[String] = None
              , hreflang: Option[String] = None
              , title: Option[String] = None)