import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "json-hal"
  val appVersion      = "0.9.3"

  val appDependencies = Seq(
    "org.scalaz" %% "scalaz-core" % "7.0.0"
  )

  val repo = if (appVersion.endsWith("SNAPSHOT"))
                Some(Resolver.file("file",  new File( "/Users/jkodumal/work/jko-repo/snapshots" )) )                    
              else
                Some(Resolver.file("file", new File("/Users/jkodumal/work/jko-repo/releases"))) 

  val customSettings = Seq[Setting[_]](
    publishTo := repo
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(customSettings:_*) 

}
